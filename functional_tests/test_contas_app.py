from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from django.urls import reverse
import time

from apps.contas.models import User


class TestContasLoginPage(StaticLiveServerTestCase):
    """Functional tests for the login Page"""

    def setUp(self):
        self.browser = webdriver.Chrome('functional_tests/chromedriver.exe')

    def tearDown(self):
        self.browser.close()

    def test_login_page_is_displayed(self):
        # user wants to login into in the application and goes to...
        self.browser.get(self.live_server_url + reverse('contas:login'))
        # time.sleep(20)

        # the user see's the this title in the browser:
        self.assertIn('r+Prof | Login', self.browser.title)
        # the user see's this message in the login page...
        tag = self.browser.find_element_by_class_name('login-box-msg')
        self.assertEquals(tag.text, 'Efetue o login para iniciar a sua sessão')

    def test_login_button_redirects_homepage(self):

        # creates a user for the test
        user = User.objects.create_user(
            username='max',
            email='max@gmail.com',
            password='#$%345#$%',
        )
        # home page url
        homepage_url = self.live_server_url + reverse('gestao_escolar:school_year')

        # in the login page ...
        self.browser.get(self.live_server_url + reverse('contas:login'))

        # the user see's the fields to enter username and password...
        username_input = self.browser.find_element_by_name("username")
        username_input.send_keys('max')
        password_input = self.browser.find_element_by_name("password")
        password_input.send_keys('#$%345#$%')

        # then he see's a login button and clicks to get login
        button = self.browser.find_element_by_css_selector('button')
        self.assertEquals(button.text, 'Login')
        button.click()

        # time.sleep(20) # to confirm the homepage redirection

        # finally the user get login and goes into homepage.
        self.assertEquals(self.browser.current_url, homepage_url)

    def test_reset_password_link_in_login_page(self):

        # reset password link
        reset_password_url = self.live_server_url + reverse('contas:password_reset')

        # in the login page
        self.browser.get(self.live_server_url + reverse('contas:login'))

        # the user see's this link to reset the password...
        links = self.browser.find_elements_by_css_selector('p a')
        for l in links:
            if l.text == 'Recupere a password':
                link = l

        # and clicks on it
        link.click()

        # then the page for reset password is displayed
        # time.sleep(20)  # to confirm the redirection
        self.assertEquals(
            self.browser.current_url,
            reset_password_url
        )
        # the user see's this button in the reset_password_page...
        button = self.browser.find_element_by_css_selector('button')
        self.assertEquals(button.text, 'Solicitar nova password')

    def test_register_link_in_login_page(self):
        # register link
        register_url = self.live_server_url + reverse('contas:register')

        # in the login page
        self.browser.get(self.live_server_url + reverse('contas:login'))

        # the user see's this link to register as anew member...
        links = self.browser.find_elements_by_css_selector('p a')
        for l in links:
            if l.text == 'Registar como novo utilizador':
                link = l

        # and clicks on it
        link.click()

        # then the register page is displayed
        # time.sleep(20)  # to confirm the redirection
        self.assertEquals(
            self.browser.current_url,
            register_url
        )
        # the user see's this button in the reset_password_page...
        button = self.browser.find_element_by_css_selector('button')
        self.assertEquals(button.text, 'Registar')


class TestContasRegisterPage(StaticLiveServerTestCase):
    """Funcitonal tests for the Register Page"""

    def setUp(self):
        self.browser = webdriver.Chrome('functional_tests/chromedriver.exe')

    def tearDown(self):
        self.browser.close()

    def test_register_page_is_displayed(self):
        # user wants to register into in the application and goes to...
        self.browser.get(self.live_server_url + reverse('contas:register'))
        # time.sleep(20)

        # the user see's the this title in the browser:
        self.assertIn('r+Prof | Registo', self.browser.title)
        # the user see's this message in the login page...
        tag = self.browser.find_element_by_class_name('login-box-msg')
        self.assertEquals(tag.text, 'Efetue o seu registo no r+Prof')

    def test_register_button_redirects_homepage(self):

        # home page url
        homepage_url = self.live_server_url + reverse('gestao_escolar:school_year')

        # in the register page ...
        self.browser.get(self.live_server_url + reverse('contas:register'))

        # the user see's the fields to register in the platform...
        username_input = self.browser.find_element_by_name("username")
        username_input.send_keys('patrocinio')
        email_input = self.browser.find_element_by_name("email")
        email_input.send_keys('patrocinio@gmail.com')
        password1_input = self.browser.find_element_by_name("password1")
        password1_input.send_keys('#$%345#$%')
        password2_input = self.browser.find_element_by_name("password2")
        password2_input.send_keys('#$%345#$%')
        self.browser.find_element_by_tag_name('label').click()

        # then he see's a register button and clicks to get register
        button = self.browser.find_element_by_css_selector('button')
        self.assertEquals(button.text, 'Registar')
        button.click()

        # time.sleep(20) # to confirm the homepage redirection

        # finally the user get registed as "patrocínio" and login at the homepage.
        self.assertEquals(self.browser.current_url, homepage_url)

    def test_if_terms_link_redirects_to_url(self):

        # terms link
        term_url = self.live_server_url + reverse('contas:terms')

        # in the register page ...
        self.browser.get(self.live_server_url + reverse('contas:register'))

        # the user clicks in te term link
        self.assertEquals(
            # get link for terms
            self.browser.find_element_by_css_selector("label > a").get_attribute('href'),
            term_url
        )


    def test_login_link_in_register_page(self):

        # login link
        login_link_url = self.live_server_url + reverse('contas:login')

        # in the register page
        self.browser.get(self.live_server_url + reverse('contas:register'))

        # the user see's this link to login page...
        link = self.browser.find_element_by_link_text('Já estou registado... efetuar o login')

        # and clicks on it
        link.click()

        # then the page for login password is displayed
        # time.sleep(20)  # to confirm the redirection
        self.assertEquals(
            self.browser.current_url,
            login_link_url
        )
