from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from django.urls import reverse
import time


class TestLandingPageApp(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome('functional_tests/chromedriver.exe')

    def tearDown(self):
        self.browser.close()

    def test_landing_page_is_displayed(self):
        # unlogged user requests the landing_page of our application
        self.browser.get(self.live_server_url)
        # time.sleep(80)

        # the user see's the message:
        page = self.browser.find_element_by_class_name('m-0')
        self.assertEquals(page.text, 'Projeto Final de Engenharia Informática!')

    def test_login_link_redirects_to_login_page(self):
        self.browser.get(self.live_server_url)
        login_url = self.live_server_url + reverse('contas:login')
        # the unlogged user try to login and access the link in the navbar
        links = self.browser.find_elements_by_css_selector('li a')
        for l in links:
            if l.text == 'Login':
                link = l
        link.click()

        self.assertEquals(
            self.browser.current_url,
            login_url
        )

    def test_register_link_redirects_to_login_page(self):
        self.browser.get(self.live_server_url)
        register_url = self.live_server_url + reverse('contas:register')
        # the unlogged user try to register and access the link in the navbar
        links = self.browser.find_elements_by_css_selector('li a')
        for l in links:
            if l.text == 'Registar':
                link = l
        link.click()

        self.assertEquals(
            self.browser.current_url,
            register_url
        )

    def test_logo_nav_bar_link_redirects_to_landing_page(self):
        self.browser.get(self.live_server_url)
        landing_url = self.live_server_url + reverse('landing_page:landing_page')
        # the unlogged try's the r+prof logo link in the navbar
        self.browser.find_element_by_css_selector('div a').click()
        self.assertEquals(
            self.browser.current_url,
            landing_url
        )
