from django.test import SimpleTestCase
from django.urls import reverse, resolve

from apps.landing_page.views import landing_page


class TestUrls(SimpleTestCase):

    def test_landing_page_is_resolves(self):
        url = reverse('landing_page:landing_page')
        self.assertEquals(resolve(url).func, landing_page)