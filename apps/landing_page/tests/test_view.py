from django.test import TestCase
from django.urls import reverse


class Landing_pageTest(TestCase):

    def test_if_url_exists_at_desired_location(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('landing_page:landing_page'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('landing_page:landing_page'))
        self.assertTemplateUsed(response, 'landing_page/landing_page.html')
