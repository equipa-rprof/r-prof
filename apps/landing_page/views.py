from django.shortcuts import render


# Create your views here.

def landing_page(request):
    template_name = 'landing_page/landing_page.html'
    message = 'Projeto Final de Engenharia Informática!'
    context = {'msn': message}

    return render(request, template_name, context)