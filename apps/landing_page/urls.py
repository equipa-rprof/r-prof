from django.urls import path

from apps.landing_page.views import landing_page

app_name = 'landing_page'

urlpatterns = [
    path('', landing_page, name='landing_page'),
]
