from django.apps import AppConfig


class GestaoEscolarConfig(AppConfig):
    name = 'apps.gestao_escolar'
    verbose_name = 'Gestão Escolar'
