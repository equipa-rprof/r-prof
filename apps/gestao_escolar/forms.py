from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.forms.models import modelformset_factory

from .models import Escola, AnoLetivo, Turma, Aluno


# https://www.geeksforgeeks.org/django-modelform-create-form-from-models/
# https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Forms


class EscolaForm(ModelForm):
    class Meta:
        model = Escola
        exclude = ['user', 'slug', 'created', 'modified', ]


def date_validation(start_date, end_date):
    # validação dos campos 'data inicial' e 'data final'
    if start_date and end_date:
        if end_date < start_date:
            raise ValidationError(
                'A data final não pode ocorrer antes da data inicial!'
            )


class AnoLetivoChangeForm(ModelForm):
    # https: // stackoverflow.com / questions / 2141030 / djangos - modelform - unique - together - validation
    class Meta:
        model = AnoLetivo
        fields = [
            'name', 'start_date', 'end_date',
        ]
        help_texts = {
            'name': ' formato: aaaa/aaaa',
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        start_date = cleaned_data['start_date']
        end_date = cleaned_data['end_date']

        if AnoLetivo.objects.exclude(pk=self.instance.pk).filter(
                name=cleaned_data['name'], user=self.instance.user).exists():
            raise ValidationError('Já tem um ano letivo com esta designação!')

        # validação dos campos 'data inicial' e 'data final'
        date_validation(start_date, end_date)

        # retorna sempre 'cleaned_data'
        return cleaned_data


class AnoLetivoAddForm(ModelForm):
    class Meta:
        model = AnoLetivo
        fields = [
            'name', 'start_date', 'end_date',
        ]
        help_texts = {
            'name': ' formato: aaaa/aaaa',
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        start_date = cleaned_data['start_date']
        end_date = cleaned_data['end_date']

        try:
            AnoLetivo.objects.get(name=cleaned_data['name'], user=self.instance.user)
        except AnoLetivo.DoesNotExist:
            # Este utilizador não tem nenhum objeto com o mesmo nome.
            # Então tudo ok. não existe erro de validação do 'unique_together'
            pass
        else:
            raise ValidationError('Já tem um ano letivo com esta designação!')

        # validação dos campos 'data inicial' e 'data final'
        date_validation(start_date, end_date)

        # retorna sempre 'cleaned_data'
        return cleaned_data

    # # outra forma de verificar o unique_together


# https://stackoverflow.com/questions/32260785/django-validating-unique-together-constraints-in-a-modelform-with-excluded-fiel


class TurmaForm(ModelForm):
    class Meta:
        model = Turma
        fields = [
            'escola', 'name', 'grade', 'subject',
            'class_type', 'dt_name', 'dt_phone', 'dt_email'
        ]


class AlunoChangeForm(ModelForm):
    class Meta:
        model = Aluno
        fields = [
            'name', 'birth_date', 'sex', 'class_number', 'class_shift',
            'email', 'phone', 'address', 'tutor_name', 'tutor_email',
            'tutor_phone', 'observations',
        ]


class AlunoFotoChangeForm(ModelForm):
    class Meta:
        model = Aluno
        fields = ['foto', ]


class AlunoAddForm(ModelForm):
    class Meta:
        model = Aluno
        fields = ['name', 'class_number']


AlunoAddMultipleFormSet = modelformset_factory(
    Aluno, fields=(
        'name',
    ), extra=1,
)
