from django.db import models
from django.urls import reverse
from datetime import date
from django.utils.text import slugify
from django.core.validators import RegexValidator
from django_resized import ResizedImageField

# phone number field
# error message when a wrong format entered
phone_message = 'Introduza um número com 9 dígitos.'
# Validation and formatting phone numbers in Classes
phone_regex = RegexValidator(
    regex=r'^\d{9}$',
    message=phone_message
)


class AnoLetivo(models.Model):
    """ Model for school_year """

    user = models.ForeignKey(
        'contas.User',
        verbose_name='Professor',
        on_delete=models.CASCADE
    )

    # regex for name validation
    name_message = 'Formato "aaaa/aaaa". Exemplo: 2020/2021'
    name_regex = RegexValidator(
        regex=r'^\d{4}\/\d{4}$',
        message=name_message,
    )
    name = models.CharField(
        'Designação',
        validators=[name_regex],
        max_length=12,
        unique=False,
        blank=False,
        help_text='Insira um ano letivo com o formato "aaaa/aaaa". Exemplo: 2020/2021'
    )
    slug = models.SlugField(
        max_length=20,
        unique=True,
        blank=True,
        help_text="Deixar em branco para criar um slug automático e único.",
    )
    start_date = models.DateField(
        'Início',
        blank=True,
        null=True,
    )
    end_date = models.DateField(
        'Fim',
        blank=True,
        null=True,
    )
    created = models.DateTimeField(
        'Criado em',
        auto_now_add=True,
    )
    modified = models.DateTimeField(
        'Modificado em',
        auto_now=True,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse(
            'gestao_escolar:school_year_change',
            kwargs={'ano_letivo_slug': self.slug})

    class Meta:
        verbose_name = 'Ano Letivo'
        verbose_name_plural = 'Anos Letivos'
        ordering = ['name']

    def save(self, *args, **kwargs):
        """set automatic and unique slug from name."""

        super(AnoLetivo, self).save(*args, **kwargs)

        if not self.slug:
            self.slug = self.name.replace("/", "-") + '_' + str(self.id)
            self.save()

    class Meta:
        """ Makes unique ano_letivo.name for the current user"""
        unique_together = ("user", "name")


class Turma(models.Model):
    """ model for student classes """

    ano_letivo = models.ForeignKey(
        'gestao_escolar.AnoLetivo',
        verbose_name='Ano Letivo',
        on_delete=models.CASCADE,
    )
    escola = models.ForeignKey(
        'gestao_escolar.Escola',
        verbose_name='Escola',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    user = models.ForeignKey(
        'contas.User',
        verbose_name='Professor',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        'Designação da turma',
        max_length=50
    )
    GRADE_OPTIONS = (
        ("5ºano", "5º Ano"), ("6ºano", "6º Ano"),
        ("7ºano", "7º Ano"), ("8ºano", "8º Ano"),
        ("9ºano", "9º Ano"), ("10ºano", "10º Ano"),
        ("11ºano", "11º Ano"), ("12ºano", "12º Ano"),
    )
    grade = models.CharField(
        'Ano',
        choices=GRADE_OPTIONS,
        max_length=100,
        default='5º Ano',
    )
    subject = models.CharField(
        'Disciplina',
        max_length=100,
        blank=True,
    )
    CLASS_TYPE_OPTIONS = (
        ("anual", "Anual"), ("semestral", "Semestral")
    )
    class_type = models.CharField(
        'Regime',
        choices=CLASS_TYPE_OPTIONS,
        max_length=100,
        default='5º Ano',
    )
    slug = models.SlugField(
        max_length=255,
        unique=True,
        blank=True,
        help_text="Deixar em branco para criar um slug automático e único.",
    )
    dt_name = models.CharField(
        'Diretor de Turma',
        max_length=100,
        blank=True,
    )
    dt_phone = models.CharField(
        'Telefone DT',
        validators=[phone_regex],
        max_length=60,
        null=True,
        blank=True,
    )
    dt_email = models.EmailField(
        'Email DT',
        max_length=254,
        blank=True,
    )
    created = models.DateTimeField(
        'Criado em',
        auto_now_add=True,
    )
    modified = models.DateTimeField(
        'Modificado em',
        auto_now=True,
    )

    class Meta:
        """options (metadata) to the model"""
        verbose_name = 'Turma'
        verbose_name_plural = 'Turmas'
        ordering = ['name']

    def __str__(self):
        """Returns the str.name from the object"""
        return self.name

    def get_absolute_url(self):
        return reverse(
            'gestao_escolar:school_class_change',
            kwargs={'ano_letivo_slug': self.ano_letivo.slug, 'turma_slug': self.slug})

    def save(self, *args, **kwargs):
        """set automatic and unique slug from name and id fields."""
        super(Turma, self).save(*args, **kwargs)
        if not self.slug:
            self.slug = slugify(self.name) + "-" + str(self.id)
            self.save()


class Aluno(models.Model):
    """ model for student """

    user = models.ForeignKey(
        'contas.User',
        verbose_name='Professor',
        on_delete=models.CASCADE,
    )
    turma = models.ForeignKey(
        'gestao_escolar.Turma',
        verbose_name='Turma',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        'Nome',
        max_length=150,
    )
    slug = models.SlugField(
        max_length=255,
        unique=True,
        blank=True,
        help_text="Deixar em branco para criar um slug automático e único.",
    )
    birth_date = models.DateField(
        'Data de Nascimento',
        blank=True,
        null=True,
    )
    SEX_CHOICES = (
        ("M", "Masculino"),
        ("F", "Feminino"),
    )
    sex = models.CharField(
        'sexo',
        choices=SEX_CHOICES,
        max_length=100,
        blank=True
        # default='M',
    )
    class_number = models.IntegerField(
        'Número de turma',
        blank=False,
    )

    CLASS_SHIFT_OPTIONS = (
        (1, "I"),
        (2, "II"),
    )
    class_shift = models.CharField(
        'Turno',
        blank=True,
        default=1,
        max_length=10,
    )
    email = models.EmailField(
        'Email',
        max_length=254,
        blank=True
    )
    phone = models.CharField(
        'Telefone',
        validators=[phone_regex],
        max_length=60,
        null=True,
        blank=True
    )
    address = models.TextField(
        'Morada',
        blank=True,
    )
    # foto = models.ImageField(
    #     'Foto',
    #     upload_to='fotos_alunos/',
    #     default='fotos_alunos/no-image.jpg'
    # )

    # https: // stackoverflow.com / questions / 57111648 / how - to - resize - an - imagefield - image - before - saving - it - in -python - django - model / 57111737
    foto = ResizedImageField(
        # com import de 'django-resized'
        size=[185, 185],
        upload_to='fotos_alunos/',
        default='fotos_alunos/no_image.png',
        crop=['middle', 'center'],
        quality=75
    )
    tutor_name = models.CharField(
        'Enc.Educação',
        max_length=100,
        blank=True,
    )
    tutor_email = models.EmailField(
        'Email EE',
        max_length=254,
        blank=True,
    )
    tutor_phone = models.CharField(
        'Telefone EE',
        validators=[phone_regex],
        max_length=60,
        null=True,
        blank=True,
    )
    observations = models.TextField(
        'Observações',
        blank=True,
    )
    created = models.DateTimeField(
        'Criado em',
        auto_now_add=True,
    )
    modified = models.DateTimeField(
        'Modificado em',
        auto_now=True,
    )

    class Meta:
        """options (metadata) to the model"""
        verbose_name = 'Aluno'
        verbose_name_plural = 'Alunos'
        ordering = ['turma']

    def get_age(self):
        """return student age from birth_date"""
        today = date.today()
        if self.birth_date:
            return today.year - self.birth_date.year - (
                (today.month, today.day) < (self.birth_date.month, self.birth_date.day)
            )

    def __str__(self):
        """Returns the str.name from the object"""
        return self.name

    def get_first_name(self):
        """get student first name"""
        return str(self).split(" ")[0]

    def get_last_name(self):
        """get student last name"""
        return str(self).split(" ")[-1]

    # def get_absolute_url(self):
    #     turma = Turma.objects.get(id=self.turma.id)
    #     ano_letivo = AnoLetivo.objects.get(id=turma.ano_letivo.id)
    #     return reverse('apps.gestao_Escolar:aluno', kwargs={
    #         'ano_letivo_slug': ano_letivo.slug,
    #         'turma_slug': turma.slug,
    #         'slug': self.slug
    #     })

    # ver o site seguinte para tentar por a funcionar
    # as imagens formatadas no tamanho:
    # https://github.com/vinyll/django-imagefit

    def save(self, *args, **kwargs):
        """set automatic and unique slug from name and id fields."""
        super(Aluno, self).save(*args, **kwargs)
        if not self.slug:
            self.slug = slugify(self.name) + "-" + str(self.id)
            self.save()


class Escola(models.Model):
    """ model for school """

    user = models.ForeignKey(
        'contas.User',
        verbose_name='Professor',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        'Designação',
        max_length=200,
    )
    slug = models.SlugField(
        max_length=255,
        unique=True,
        blank=True,
        help_text="Deixar em branco para criar um slug automático e único.",
    )
    code_message = 'O código da Escola deve ter 6 dígitos.'
    # Validation and formatting phone numbers in Classes
    code_regex = RegexValidator(
        regex=r'^\d{6}$',
        message=code_message
    )
    code = models.CharField(
        'Código da Escola',
        validators=[code_regex],
        max_length=10,
        blank=True,
        unique=True,
        null=True,
        help_text='Introduza um código com 6 dígitos.'
    )
    address = models.TextField(
        'Morada',
        blank=True
    )
    phone = models.CharField(
        'Telefone',
        validators=[phone_regex],
        max_length=60,
        null=True,
        blank=True
    )
    admin_email = models.EmailField(
        'Email Direção',
        max_length=254,
        blank=True,
    )
    services_email = models.EmailField(
        'Email Secretaria',
        max_length=254,
        blank=True,
    )
    created = models.DateTimeField(
        'Criado em',
        auto_now_add=True,
    )
    modified = models.DateTimeField(
        'Modificado em',
        auto_now=True,
    )

    class Meta:
        """options (metadata) to the model"""
        verbose_name = 'Escola'
        verbose_name_plural = 'Escolas'
        ordering = ['name']

    def __str__(self):
        """Returns the str.name from the object"""
        return self.name

    def get_absolute_url(self):
        return reverse(
            'gestao_escolar:school_change',
            kwargs={'escola_slug': self.slug}
        )

    def save(self, *args, **kwargs):
        """set automatic and unique slug from name and id fields."""
        super(Escola, self).save(*args, **kwargs)
        if not self.slug:
            self.slug = slugify(self.name) + "-" + str(self.id)
            self.save()
