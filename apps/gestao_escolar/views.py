from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

from apps.gestao_escolar.models import AnoLetivo, Turma, Aluno, Escola
from .forms import EscolaForm, AnoLetivoAddForm, AnoLetivoChangeForm, TurmaForm, AlunoChangeForm, AlunoFotoChangeForm, \
    AlunoAddForm, AlunoAddMultipleFormSet


# importante ver isto
# unique slug
# https://stackoverflow.com/questions/21023813/django-saving-unique-entries-by-automatically-appending-a-counter-to-the-title

# Create your views here.
@login_required
def homepage(request, ano_letivo_slug=None):
    """
    Apresenta a página inicial do utilizador com login efetuado.
    -> Se forem passados dois argumentos (user e ano_letivo) apresenta as turmas do ano letivo selecionado.
    -> Se for passado um argumento (user) apresenta as turmas do último ano letivo modificado.
    -> Se for passado um argumento (user) e não existirem anos letivos (1ª utilização da plataforma ou livro em branco)
       apresenta html com indicações para o início do registo de turmas.
    """

    if ano_letivo_slug is not None:
        """ 
        apresenta as turmas do ano letivo selecionado no argumento 'ano_letivo'
        quando o utilizador pede para ver um ano letivo especifico
        passa esse argumento no 'slug' 
        """
        ano_letivo_corrente = AnoLetivo.objects.get(
            user=request.user,
            slug=ano_letivo_slug,
        )
    else:
        try:
            """
            Neste caso não há 'slug' passado no argumento da função.
            Verifica se o utilizador têm anos letivos registados na plataforma e 
            devolve o último que foi alterado. 
            """
            ano_letivo_corrente = AnoLetivo.objects.filter(user=request.user).latest('modified')

        except ObjectDoesNotExist:
            """
            Senão, recupera do erro anterior... é a primeira utilização do programa, não existem
            registos de Anos letivos. Apresenta HTML com indicações para a criação desses objetos.
            """
            template_name = 'gestao_escolar/homepage_first_use.html'
            return render(request, template_name)

        """
        se o utilizador registos de anos letivos, atualiza regista o ano letivo da ultima turma modificada e
        chama novamente a função com os argumentos atualizados
        """
        return redirect(
            'gestao_escolar:school_class_list',
            ano_letivo_slug=ano_letivo_corrente.slug
        )

    template_name = 'gestao_escolar/homepage.html'

    quantidade_escolas = Escola.objects.filter(
        user=request.user,
    ).count()

    anos_letivos_barra_lateral = AnoLetivo.objects.filter(
        user=request.user,
    ).order_by('-modified')[:3]
    quantidade_anos_letivos = AnoLetivo.objects.filter(
        user=request.user,
    ).count()

    try:
        turmas = Turma.objects.filter(
            user=request.user,
            ano_letivo=ano_letivo_corrente
        )
        quantidade_turmas = turmas.count()
    except ObjectDoesNotExist:
        turmas = None
        quantidade_turmas = 0

    context = {
        'ano_letivo_corrente': ano_letivo_corrente,
        'anos_letivos_side_bar': anos_letivos_barra_lateral,
        'quantidade_escolas': quantidade_escolas,
        'anos_letivos_count': quantidade_anos_letivos,
        'turmas': turmas,
        'quantidade_turmas': quantidade_turmas,
        # 'dicionario_turmas': dicionario_turmas,
        # 'disciplinasTurma': disciplinasTurma,
        # 'dicionario_disciplinas_turmas': dicionario_disciplinas_turmas,
        # 'alunos': alunos,
        # 'total_alunos': total_alunos,
        # 'alunOs': alunOs,
        # 'alunAs': alunAs,
    }

    return render(request, template_name, context)


# *** Escola_views ***********************************************************************

def side_bar_func(request, context):
    quantidade_escolas = Escola.objects.filter(
        user=request.user,
    ).count()

    anos_letivos_barra_lateral = AnoLetivo.objects.filter(
        user=request.user,
    ).order_by('-modified')[:3]
    quantidade_anos_letivos = AnoLetivo.objects.filter(
        user=request.user,
    ).count()

    context.update({
        'anos_letivos_side_bar': anos_letivos_barra_lateral,
        'quantidade_escolas': quantidade_escolas,
        'anos_letivos_count': quantidade_anos_letivos,
    })

    return context


@login_required
def school_list(request):
    """
    Apresenta a lista de todas as escolas do utilizador corrente.
    # link => side_bar -> botão Escolas
    # url -> path("escola/lista", views.school_list, name="school_list"),
    """

    template_name = 'gestao_escolar/school_list.html'
    context = {}

    try:
        escola_list = Escola.objects.all().filter(user=request.user)
        sl = {'school_list': escola_list}
        context.update(sl)

    except ObjectDoesNotExist:
        msg = {'message': 'Ainda não tem escolas registadas!!'}
        context.update(msg)

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def school_add(request):
    """
        Apresenta formulario para adição de escola.
        # link => na lista de escolas -> link para adicionar
        # path("escola/criar", views.school_add, name="school_add"),
    """

    form = EscolaForm(request.POST or None)
    if form.is_valid():
        school = form.save(commit=False)
        school.user_id = request.user.id
        school.save()
        messages.success(request, f"'Escola '{school.name}' adicionada com sucesso ")
        return redirect('gestao_escolar:school_list')

    template_name = 'gestao_escolar/school_add.html'
    context = {'form': form}

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def school_change(request, escola_slug):
    """
        Apresenta os dados de uma escola e permite que o utilizador os altere.
        # link => cartão de cada escola no -> more info
        # path("escola/<escola_slug>/remover", views.school_delete, name="school_delete"),,
    """

    school = get_object_or_404(Escola, slug=escola_slug)
    form = EscolaForm(request.POST or None, instance=school)
    if form.is_valid():
        form.save()
        return redirect('gestao_escolar:school_list')

    template_name = 'gestao_escolar/school_change.html'
    context = {'form': form,
               'school': school
               }

    context = side_bar_func(request, context)

    return render(request, template_name, context)


# https://stackoverflow.com/questions/46003056/how-to-make-delete-button-in-django/46006826
@login_required
def school_delete(request, escola_slug):
    """
    delete a school object
    link - página com um botão a dizer " pretende mesmo remover esta escola?"
    path("escola/<escola_slug>/remover", views.school_delete, name="school_delete"),
    """

    school = get_object_or_404(Escola, slug=escola_slug)

    if request.method == 'POST':
        messages.success(request, f"'Escola '{school.name}' removida com sucesso ")
        school.delete()
        return redirect('gestao_escolar:school_list')

    template_name = 'gestao_escolar/school_delete.html'
    context = {'school': school}

    context = side_bar_func(request, context)

    return render(request, template_name, context)


# *** Ano_Letivo_views ***********************************************************************

@login_required
def school_year_list(request):
    """
    Apresenta a lista de todos os anos letivos do utilizador corrente.
    # link => side_bar -> botão Anos Letivos -> Ver todos.
    # path("ano-letivo/lista/", views.school_year_list, name="school_year_list"),
    """
    template_name = 'gestao_escolar/school_year_list.html'
    context = {}

    try:
        ano_letivo_list = AnoLetivo.objects.all().filter(user=request.user)
        syl = {'school_year_list': ano_letivo_list}
        context.update(syl)

    except ObjectDoesNotExist:
        msg = {'message': 'Ainda não tem Anos Letivos registados!!'}
        context.update(msg)

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def school_year_add(request):
    """
        Apresenta formulário para adição de um ano letivo.
        # link => barra lateral -> link para adicionar
        # path("ano-letivo/criar/", views.school_year_add, name="school_year_add"),
    """
    ano_letivo = AnoLetivo()
    ano_letivo.user = request.user
    form = AnoLetivoAddForm(request.POST or None, instance=ano_letivo)
    if request.method == 'POST':
        if form.is_valid():
            ano_letivo = form.save(commit=False)
            ano_letivo.user_id = request.user.id
            ano_letivo.save()
            messages.success(request, f"'Ano Letivo '{ano_letivo.name}' adicionado com sucesso ")
            return redirect('gestao_escolar:school_year_list')
        else:
            messages.error(request, "Corrija os erros abaixo indicados!")

    template_name = 'gestao_escolar/school_year_add.html'
    context = {'form': form}

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def school_year_change(request, ano_letivo_slug):
    """
        Apresenta os dados de um ano letivo e permite que o utilizador os altere.
        # link => cartão de cada ano letivo no -> more info
        # path("ano-letivo/<ano_letivo_slug>/alterar", views.school_year_change, name="school_year_change"),
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)
    form = AnoLetivoChangeForm(request.POST or None, instance=ano_letivo)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('gestao_escolar:school_year_list')
        else:
            messages.error(request, "Corrija os erros abaixo indicados!")

    template_name = 'gestao_escolar/school_year_change.html'
    context = {'form': form,
               'school_year': ano_letivo, }

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def school_year_delete(request, ano_letivo_slug):
    """
        delete a school object
        link - página com um botão a dizer " pretende mesmo remover esta escola?"
        path("ano-letivo/<ano_letivo_slug>/remover", views.school_year_delete, name="school_year_delete"),
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)

    if request.method == 'POST':
        messages.success(request, f"'Ano Letivo '{ano_letivo.name}' removido com sucesso ")
        ano_letivo.delete()

        return redirect('gestao_escolar:school_year_list')

    template_name = 'gestao_escolar/school_year_delete.html'
    context = {'school_year': ano_letivo}

    context = side_bar_func(request, context)

    return render(request, template_name, context)


# ******** Turmas ************************************************

@login_required
def school_class_change(request, ano_letivo_slug, turma_slug):
    """
        Apresenta os dados de uma turma e permite que o utilizador os altere.
        #  ?? link
        # path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/alterar", views.school_class_change,
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)
    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo)
    form = TurmaForm(request.POST or None, instance=turma)
    if form.is_valid():
        form.save()
        return redirect('gestao_escolar:school_class_list', ano_letivo_slug=ano_letivo_slug)

    template_name = 'gestao_escolar/school_class_change.html'
    context = {'form': form,
               'school_year': ano_letivo,
               'school_class': turma}

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def school_class_add(request, ano_letivo_slug):
    """
        Apresenta formulário para adição de uma turma.
        # link => barra lateral -> link para adicionar turma ... e na pagina listar turmas
        # path("ano-letivo/<ano_letivo_slug>/turma/criar", views.school_class_add, name="school_class_add"),
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)
    turma = Turma()
    turma.user = request.user
    turma.ano_letivo_id = ano_letivo.id
    form = TurmaForm(request.POST or None, instance=turma)
    if request.method == 'POST':
        if form.is_valid():
            turma = form.save(commit=False)
            turma.save()
            messages.success(request, f"'Turma '{turma.name}' adicionada com sucesso ")
            return redirect('gestao_escolar:school_class_list', ano_letivo_slug=ano_letivo_slug)
        else:
            messages.error(request, "Corrija os erros abaixo indicados!")

    template_name = 'gestao_escolar/school_class_add.html'
    context = {'form': form}

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def school_class_delete(request, ano_letivo_slug, turma_slug):
    """
        delete a school_class object
        link - página com um botão a dizer " pretende mesmo remover esta turma?"
        path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/remover", views.school_class_delete,
        name="school_class_delete"),
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)
    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo)
    if request.method == 'POST':
        messages.success(request, f"'Turma '{turma.name}' removida com sucesso ")
        turma.delete()
        return redirect('gestao_escolar:school_class_list', ano_letivo_slug=ano_letivo_slug)

    template_name = 'gestao_escolar/school_class_delete.html'
    context = {'school_class': turma,
               'school_year': ano_letivo}

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def school_class_detail(request, ano_letivo_slug, turma_slug):
    """
        Apresenta a página com as informações da turma: lista de alunos, avaliações e outros links úteis.
        # link => lista de turmas ou side_bar -> clicar nome da turma.
        # path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>", views.school_class_detail,
        name="school_class_detail"),
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug, user=request.user)

    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo, user=request.user)

    alunos = Aluno.objects.filter(user=request.user, turma=turma).order_by('class_number')
    alunos_masc = Aluno.objects.filter(user=request.user, turma=turma, sex='M')
    alunos_fem = Aluno.objects.filter(user=request.user, turma=turma, sex='F')

    total_alunos = alunos.count()
    total_alunos_masc = alunos_masc.count()
    total_alunos_fem = alunos_fem.count()

    context = {
        'school_year': ano_letivo,                  # objeto ano_letivo
        'school_class': turma,                      # objeto turma
        'students': alunos,                         # lista de alunos
        'masc_students': alunos_masc,               # lista de alunos masculinos
        'fem_students': alunos_fem,                 # lista de alunos femininos
        'total_students': total_alunos,             # total de alunos na turma
        'total_M_students': total_alunos_masc,      # total de alunOs
        'total_F_students': total_alunos_fem,       # total de alunAs
        }

    template_name = 'gestao_escolar/school_class_detail.html'
    context = side_bar_func(request, context)

    return render(request, template_name, context)


# *** Alunos_views ***********************************************************************

@login_required
def student_add(request, ano_letivo_slug, turma_slug):
    """
        Apresenta formulário para adição de um aluno.
        # link => página da turma -> tabela de alinos -> link para adicionar aluno
        # path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/criar", views.student_add, name="student_add"),
    """

    user = request.user
    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug, user=request.user)
    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo, user=request.user)

    try:
        escola = Escola.objects.get(id=turma.escola_id)
    except ObjectDoesNotExist:
        escola = None

    aluno = Aluno()
    aluno.user = user
    aluno.turma = turma

    form = AlunoAddForm(request.POST or None, instance=aluno)
    if request.method == 'POST':
        if form.is_valid():
            # aluno = form.save(commit=False)
            aluno.save()
            messages.success(request, f"'Aluno(a) '{aluno.name}' adicionado(a) com sucesso ")
            return redirect('gestao_escolar:school_class_detail', ano_letivo_slug=ano_letivo_slug, turma_slug=turma_slug)
        else:
            messages.error(request, "Corrija os erros abaixo indicados!")

    template_name = 'gestao_escolar/student_add.html'
    context = {'form': form}

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def student_add_multiple(request, ano_letivo_slug, turma_slug):
    """
        Apresenta formulário para adição de um aluno.
        # link => página da turma -> tabela de alunos -> link para adicionar vários aluno
        # path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/criar", views.student_add, name="student_add"),
    """

    # Para definição de utilizador, ano letivo e turma dos alunos a registar.
    user = request.user
    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug, user=request.user)
    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo, user=request.user)

    if request.method == 'GET':
        # Para apresentar, no formulário, todos os alunos registados na turma.
        # formset = AlunoAddManyFormSet()
        # Não apresenta, no formulário, os alunos já registados na turma
        formset = AlunoAddMultipleFormSet(queryset=Aluno.objects.none())

    elif request.method == 'POST':
        formset = AlunoAddMultipleFormSet(request.POST, queryset=Aluno.objects.all())

        numero_inicial_alunos = Aluno.objects.filter(user=user, turma=turma).count()
        total_alunos = numero_inicial_alunos
        if formset.is_valid():
            for form in formset:
                if form.cleaned_data.get('name'):
                    # só grava alunos com o campo 'nome' do formulário preenchido
                    total_alunos = total_alunos + 1         # contagem de alunos na turma

                    # suspende a gravação para atualização de campos no objeto aluno
                    aluno = form.save(commit=False)
                    aluno.user = user                       # definição de utilizador
                    aluno.turma = turma                     # definição de turma
                    aluno.class_number = total_alunos       # definição de número de turma
                    aluno.save()                            # gravação de aluno na Base de Dados

            # processamento de mensagens de sucesso, aviso e erro para o utilizador
            novos_alunos = total_alunos - numero_inicial_alunos
            if novos_alunos == 0:
                messages.warning(request, f"Nenhum aluno adicionado ")
            elif novos_alunos == 1:
                messages.success(request, f"{novos_alunos} aluno adicionado com sucesso ")
            else:
                messages.success(request, f"{novos_alunos} alunos adicionados com sucesso ")
            return redirect('gestao_escolar:school_class_detail', ano_letivo_slug=ano_letivo_slug, turma_slug=turma_slug)
        else:
            messages.error(request, "Corrija os erros abaixo indicados!")

    template_name = 'gestao_escolar/student_add_multiple.html'
    context = {'formset': formset, }
    return render(request, template_name, context)


@login_required
def student_detail(request, ano_letivo_slug, turma_slug, aluno_slug):
    """
        Apresenta os dados de um aluno para consulta.
        #  link -> na lista de alunos da turma -> clicar no nome do aluno
        path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/<aluno_slug>", views.student_detail,
        name="student_detail"),
    """
    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)
    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo)
    aluno = get_object_or_404(Aluno, slug=aluno_slug, turma=turma)

    template_name = 'gestao_escolar/student_detail.html'
    context = {
        'school_year': ano_letivo,      # objeto ano letivo
        'school_class': turma,          # objeto turma
        'student': aluno,               # objeto aluno
    }

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def student_change(request, ano_letivo_slug, turma_slug, aluno_slug):
    """
        Apresenta os dados de uma turma e permite que o utilizador os altere.
        #  ?? link
        path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/<aluno_slug>/alterar", views.student_change,
        name="student_change"),
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)
    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo)
    aluno = get_object_or_404(Aluno, slug=aluno_slug, turma=turma)
    form = AlunoChangeForm(request.POST or None, instance=aluno)
    if form.is_valid():
        form.save()
        return redirect('gestao_escolar:school_class_detail', ano_letivo_slug=ano_letivo_slug, turma_slug=turma_slug)

    template_name = 'gestao_escolar/student_change.html'
    context = {
            'form': form,
            'school_year': ano_letivo,
            'school_class': turma,
            'student': aluno,
        }

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def student_foto_change(request, ano_letivo_slug, turma_slug, aluno_slug):
    """
        Apresenta os dados de uma turma e permite que o utilizador os altere.
        #  ?? link
        path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/<aluno_slug>/alterar", views.student_change,
        name="student_change"),
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)
    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo)
    aluno = get_object_or_404(Aluno, slug=aluno_slug, turma=turma)
    form = AlunoFotoChangeForm(instance=aluno)

    if request.method == 'POST':
        form = AlunoFotoChangeForm(request.POST, request.FILES, instance=aluno)
        if form.is_valid():
            form.save()
            return redirect('gestao_escolar:school_class_detail', ano_letivo_slug=ano_letivo_slug, turma_slug=turma_slug)

    template_name = 'gestao_escolar/student_foto_change.html'
    context = {
            'form': form,
            'school_year': ano_letivo,
            'school_class': turma,
            'student': aluno,
        }

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def student_delete(request, ano_letivo_slug, turma_slug, aluno_slug):
    """
        delete a student object
        link - página com um botão a dizer " pretende mesmo remover este aluno?"
        path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/<aluno_slug>/remover", views.student_delete,
        name="student_delete"),
    """

    ano_letivo = get_object_or_404(AnoLetivo, slug=ano_letivo_slug)
    turma = get_object_or_404(Turma, slug=turma_slug, ano_letivo=ano_letivo)
    aluno = get_object_or_404(Aluno, slug=aluno_slug, turma=turma)
    if request.method == 'POST':
        messages.success(request, f"'Aluno(a) '{aluno.name}' removido(a) com sucesso ")
        aluno.delete()
        return redirect('gestao_escolar:school_class_detail', ano_letivo_slug=ano_letivo_slug, turma_slug=turma_slug)

    template_name = 'gestao_escolar/student_delete.html'
    context = {
            'school_class': turma,
            'school_year': ano_letivo,
            'student': aluno,
        }

    context = side_bar_func(request, context)

    return render(request, template_name, context)
