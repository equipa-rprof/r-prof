from django.contrib import admin
from django.utils.text import slugify
from .models import AnoLetivo, Turma, Aluno, Escola


# class PostAdmin(admin.ModelAdmin):
#     list_display = ["id", "__str__", "publishing_date", "updating_date", "category", "highlighted"]
#     list_filter = ["publishing_date"]
#     search_fields = ["title", "short_description", "contents", "keyconcept", "category"]
#     prepopulated_fields = {"slug": ("title", "keyconcept", "category",)}
#
#     class Meta:
#         model = Post
#
# admin.site.register(Post, PostAdmin)
# admin.site.register(KeyConcept)
# admin.site.register(Argument)

class AnoLetivoAdmin(admin.ModelAdmin):
    """ Definições para entradas de Anos Letivos no Admin"""

    # prepopulated_fields = {'slug': ('name',), }

    list_display = ['user', 'name', 'slug', 'created', 'modified']
    search_fields = ['user', 'name', 'slug', 'created', 'modified']

    list_filter = ['user', 'name', 'slug', 'created', 'modified']


admin.site.register(AnoLetivo, AnoLetivoAdmin)


class TurmaAdmin(admin.ModelAdmin):
    """ Criação da Tabela da turma no admin """

    list_display = [
        'name', 'get_escola', 'grade', 'get_ano_letivo',
        'user', 'dt_name', 'dt_phone', 'dt_email',
    ]
    search_fields = [
        'name', 'get_escola', 'grade', 'get_ano_letivo',
        'user', 'dt_name', 'dt_phone', 'dt_email',
    ]
    list_filter = [
        'name', 'escola', 'grade', 'ano_letivo',
        'user', 'dt_name', 'dt_phone', 'dt_email',
    ]

    def get_escola(self, obj):
        """ get school object """
        if obj.escola:
            return obj.escola.name
        else:
            return None

    def get_ano_letivo(self, obj):
        """ get school object """
        if obj.ano_letivo:
            return obj.ano_letivo.name
        else:
            return None

    # Allows column order sorting
    get_escola.admin_order_field = 'escola'
    # Renames column head
    get_escola.short_description = 'Escola'
    get_ano_letivo.short_description = 'Ano Letivo'


admin.site.register(Turma, TurmaAdmin)


# Todo feito
# https://stackoverflow.com/questions/11978035/django-unique-slug-by-id
# https://stackoverflow.com/questions/30169018/how-do-i-customize-a-slug-field-in-django


class AlunoAdmin(admin.ModelAdmin):
    """ Criação da Tabela do aluno no admin """

    # prepopulated_fields = {'slug': ('nome', 'turma', ), }

    list_display = [
        'name', 'user', 'get_turma', 'class_number', 'birth_date', 'tutor_name', 'tutor_phone',
    ]
    search_fields = [
        'name', 'user', 'get_turma', 'class_number', 'birth_date', 'tutor_name',
    ]
    list_filter = [
        'name', 'user', 'class_number', 'turma', 'birth_date', 'tutor_name',
    ]

    def get_turma(self, obj):
        return obj.turma.name

    # Allows column order sorting
    get_turma.admin_order_field = 'turma'
    # Renames column head
    get_turma.short_description = 'Turma'


admin.site.register(Aluno, AlunoAdmin)


class EscolaAdmin(admin.ModelAdmin):
    """ tabela com as informações das escolas no admin"""

    # prepopulated_fields = {'slug': ('nome',), }

    list_display = [
        'user', 'name', 'code', 'address', 'phone', 'created', 'modified'
    ]
    search_fields = [
        'user', 'name', 'code', 'address', 'phone', 'created', 'modified'
    ]
    list_filter = [
        'user', 'name', 'code', 'address', 'phone', 'created', 'modified'
    ]


admin.site.register(Escola, EscolaAdmin)
