from django.core.management.base import BaseCommand, CommandError

from apps.gestao_escolar.models import Escola, AnoLetivo, Turma, Aluno
from apps.contas.models import User


class Command(BaseCommand):
    help = 'Carrega a base de dados com objetos do tipo escola'

    def add_arguments(self, parser):
        parser.add_argument('object', nargs='*', type=str, default='all',
                            help='Tipo de objeto a inserir na base de dados')

    def handle(self, *args, **kwargs):
        help = 'Carrega Escolas, Anos letivos e Alunos para a Base de dados'

        # caminho para o ficheiro
        path = 'apps/gestao_escolar/utils/rprof_data.txt'

        # tenta abrir o ficheiro txt na localização indicada
        try:
            file_data = open(path, encoding='utf8')
        except FileNotFoundError:
            self.stdout.write(self.style.ERROR('\nERRO: Ficheiro não encontrado em -> %s' % path))
            return

        objects = kwargs['object']
        print(objects)

        if objects == 'all' or objects[0] == 'all':
            print('aqui')
            # grava todos os objetos do ficheiro de dados na Base de Dados
            for line in file_data.readlines():
                model_fields = line.strip().split(';')

                self.save_object_in_db(model_fields)

        else:
            for line in file_data.readlines():
                model_fields = line.strip().split(';')

                for obj in objects:
                    if model_fields[0] == obj:
                        self.save_object_in_db(model_fields)

    def save_object_in_db(self, fields):

        if fields[0] == 'E':
            # modelo escola
            # fields = [ class_name, username, name, code, address, phone ]
            try:
                user = User.objects.get(username=fields[1])
                Escola.objects.create(
                    user=user,
                    name=fields[2],
                    code=fields[3],
                    address=fields[4],
                    phone=fields[5]
                )
                self.stdout.write(
                    self.style.SUCCESS('Escola "%s (%s)" criada com sucesso!' % (fields[2], user)))
            except Exception as e:
                self.stdout.write(self.style.ERROR('%s (%s)' % (e, type(e))))

        if fields[0] == 'AL':
            # modelo Ano Letivo
            # fields = [ class_name, username, name, start_date, end_date ]
            try:
                user = User.objects.get(username=fields[1])
                AnoLetivo.objects.create(
                    user=user,
                    name=fields[2],
                    start_date=fields[3],
                    end_date=fields[4],
                )
                self.stdout.write(
                    self.style.SUCCESS('Ano Letivo "%s (%s)" criado com sucesso!' % (fields[2], user)))
            except Exception as e:
                self.stdout.write(self.style.ERROR('%s (%s)' % (e, type(e))))

        if fields[0] == 'T':
            # modelo Turma
            # fields = [
            #   class_name, ano_letivo, escola, username, name,
            #   grade, subject, class_type, dt_name, dt_phone; de_email
            # ]
            try:
                user = User.objects.get(username=fields[3])
                ano_letivo = AnoLetivo.objects.get(name=fields[1])
                escola = Escola.objects.get(name=fields[2])
                Turma.objects.create(
                    ano_letivo=ano_letivo,
                    escola=escola,
                    user=user,
                    name=fields[4],
                    grade=fields[5],
                    subject=fields[6],
                    class_type=fields[7],
                    dt_name=fields[8],
                    dt_phone=fields[9],
                    dt_email=fields[10],
                )
                self.stdout.write(
                    self.style.SUCCESS('Turma "%s (%s)" criada com sucesso!' % (fields[4], user)))
            except Exception as e:
                self.stdout.write(self.style.ERROR('%s (%s)' % (e, type(e))))

        if fields[0] == 'A':
            # modelo Aluno
            # fields = [
            #   class_name, username, turma, name, class_number
            # ]
            try:
                user = User.objects.get(username=fields[1])
                turma = Turma.objects.get(name=fields[2])
                Aluno.objects.create(
                    user=user,
                    turma=turma,
                    name=fields[3],
                    class_number=int(fields[4]),
                )
                self.stdout.write(
                    self.style.SUCCESS(
                        'Aluno(a) "%s - %s  (%s)" criado(a) com sucesso!' % (fields[4], fields[3], user)
                    )
                )
            except Exception as e:
                self.stdout.write(self.style.ERROR('%s (%s)' % (e, type(e))))


# Consultas:
# https://stackoverflow.com/questions/16853649/how-to-execute-a-python-script-from-the-django-shell
# https://docs.djangoproject.com/en/2.2/howto/custom-management-commands/
# https://simpleisbetterthancomplex.com/tutorial/2018/08/27/how-to-create-custom-django-management-commands.html
# https://docs.djangoproject.com/en/2.2/howto/initial-data/

# read from file
# https://stackoverflow.com/questions/51737750/how-to-get-values-from-a-txt-file-into-a-database-django
