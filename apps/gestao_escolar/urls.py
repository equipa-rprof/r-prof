from apps.gestao_escolar import views
from django.urls import path

app_name = 'gestao_escolar'

urlpatterns = [

    path("", views.homepage, name="homepage_blank"),
    path("ano-letivo/lista/", views.school_year_list, name="school_year_list"),
    path("ano-letivo/criar/", views.school_year_add, name="school_year_add"),
    path("ano-letivo/<ano_letivo_slug>/alterar", views.school_year_change, name="school_year_change"),
    path("ano-letivo/<ano_letivo_slug>/remover", views.school_year_delete, name="school_year_delete"),

    path("ano-letivo/", views.homepage, name="school_year"),

    path("ano-letivo/<ano_letivo_slug>/turma/lista", views.homepage, name="school_class_list"),

    path("ano-letivo/<ano_letivo_slug>/turma/criar", views.school_class_add, name="school_class_add"),
    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>", views.school_class_detail, name="school_class_detail"),
    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/alterar", views.school_class_change,
         name="school_class_change"),
    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/remover", views.school_class_delete,
         name="school_class_delete"),
    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/criar", views.student_add,
         name="student_add"),
    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/criar_alunos", views.student_add_multiple,
         name="student_add_multiple"),

    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/<aluno_slug>", views.student_detail,
         name="student_detail"),
    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/<aluno_slug>/alterar", views.student_change,
         name="student_change"),
    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/<aluno_slug>/alterar_foto", views.student_foto_change,
         name="student_foto_change"),
    path("ano-letivo/<ano_letivo_slug>/turma/<turma_slug>/aluno/<aluno_slug>/remover", views.student_delete,
         name="student_delete"),

    path("escola/lista", views.school_list, name="school_list"),
    path("escola/criar", views.school_add, name="school_add"),
    path("escola/<escola_slug>/alterar", views.school_change, name="school_change"),
    path("escola/<escola_slug>/remover", views.school_delete, name="school_delete"),
]
