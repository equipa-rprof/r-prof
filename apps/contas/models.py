from django.core.validators import RegexValidator, re
from django.db import models
from django.core import validators
from django.template.defaultfilters import slugify
from django_resized import ResizedImageField

from django.contrib.auth.models import (
    AbstractBaseUser, UserManager, PermissionsMixin
)


# Documentação consultada
# https://docs.djangoproject.com/en/2.2/ref/contrib/auth/

# código fonte 'class User'
# https://github.com/django/django/blob/main/django/contrib/auth/models.py

class User(AbstractBaseUser, PermissionsMixin):
    """Modelo base para os utilizadores"""

    username = models.CharField(
        'Username', max_length=30, unique=True, validators=[
            validators.RegexValidator(
                re.compile('^[\w.@+-]+$'),
                'Indique um username válido. '
                'Este campo deve conter apenas letras, números '
                'e os carateres: @/./+/-/_.',
                'inválido'
            )
        ], help_text='Um nome curto para identificá-lo de forma única no site'
    )
    email = models.EmailField('Email', max_length=254, unique=True)
    slug = models.SlugField('caminho', max_length=255, null=False, unique=True)

    first_name = models.CharField('Nome', max_length=100, blank=True)
    last_name = models.CharField('Apelido', max_length=100, blank=True)
    address = models.CharField('Morada', max_length=250, blank=True)

    # error message when a wrong format entered
    phone_message = 'Introduza um número com 9 dígitos.'
    # regex for phone validation
    phone_regex = RegexValidator(
        regex=r'^\d{9}$',
        message=phone_message
    )
    phone = models.CharField(
        'Telefone',
        validators=[phone_regex],
        max_length=60,
        null=True,
        blank=True
    )

    # avatar = models.ImageField(
    #     'Foto', upload_to='media/fotos_profs/',
    #     default='media/fotos_profs/no-img.jpg'
    # )

    avatar = ResizedImageField(
        size=[185, 185],
        upload_to='fotos_users/',
        default='fotos_users/no_image.png',
        crop=['middle', 'center'],
        quality=75
    )

    is_active = models.BooleanField('Ativo', default=True)
    is_staff = models.BooleanField('Administrador', default=False)
    created = models.DateTimeField('Criado em', auto_now_add=True)
    modified = models.DateTimeField('Modificado em', auto_now=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    class Meta:
        verbose_name = 'Utilizador'
        verbose_name_plural = 'Utilizadores'
        ordering = ['first_name']

    def __str__(self):
        return self.first_name or self.username

    def get_full_name(self):
        """ Retorna o o primeiro nome mais o segundo nome com um espaço no meio.
        função strip() retira espaços no início e no fim da string."""
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    get_full_name.short_description = 'Nome'

    def get_short_name(self):
        return self.first_name

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        if not self.slug:
            self.slug = slugify(self.username)
            self.save()

# atualizar o campo número de telefone... verificar se isto funciona
# https://stackoverflow.com/questions/59480134/how-to-store-a-phone-number-in-django-model


# referências consultadas

# exemplos de user.model
# https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#abstractuser

# documentação
# https://docs.djangoproject.com/en/2.2/ref/contrib/auth/

# código fonte 'class User'
# https://github.com/django/django/blob/main/django/contrib/auth/models.py


# *** Consulted documentation ****
# Writing and running tests:
# https://docs.djangoproject.com/en/2.2/topics/testing/overview/

# Class RequestFactory:
# https://docs.djangoproject.com/en/2.2/topics/testing/advanced/

# Customizing authentication in Django:
# https://docs.djangoproject.com/en/2.2/topics/auth/customizing/
