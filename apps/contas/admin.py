# Registo dos modelos no Admin do Django
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import User
from .forms import UserAdminCreationForm, UserAdminForm


class UserAdmin(BaseUserAdmin):
    add_form = UserAdminCreationForm
    add_fieldsets = (
        (None, {
            'fields': ('username', 'first_name', 'last_name', 'email', 'avatar',
                       'password1', 'password2')
        }),
    )
    form = UserAdminForm
    fieldsets = (
        (None, {
            'fields': ('username', 'email', 'avatar', 'password', 'slug')
        }),
        ('Informações Básicas', {
            'fields': ('first_name', 'last_name', 'address', 'phone', 'last_login')
        }),
        (
            'Permissões', {
                'fields': (
                    'is_active', 'is_staff', 'is_superuser', 'groups',
                    'user_permissions'
                )
            }
        ),
    )
    list_display = [
        'username', 'get_full_name', 'email', 'address', 'phone', 'avatar',
        'is_active', 'is_staff'
    ]

    list_filter = [
        'username', 'first_name', 'last_name', 'email', 'address', 'phone',
        'is_active', 'is_staff', 'created', 'modified'
    ]

    search_fields = [
        'username', 'first_name', 'last_name', 'email', 'address', 'mobile', 'foto',
        'is_active', 'is_staff'
    ]


admin.site.register(User, UserAdmin)