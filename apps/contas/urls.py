from django.conf.urls import url
from apps.contas import views
from django.urls import path, reverse_lazy
# Utilizado para envio de emails de recuperação de password
from django.contrib.auth import views as auth_views

app_name = 'contas'

urlpatterns = [
    path("login/", views.login_view, name="login"),
    path("logout/", views.logout_view, name="logout"),
    path("register/", views.register, name="register"),
    path("terms/", views.terms, name="terms"),

    # update user profile
    path("change_password/", views.change_password, name="change_password"),
    path("perfil/<user_slug>/alterar", views.user_profile_change, name="user_profile_change"),
    path("perfil/<user_slug>/alterar_foto", views.user_profile_foto_change, name="user_profile_foto_change"),

    # reset password
    path('reset_password/', auth_views.PasswordResetView.as_view(
        template_name="contas/reset_password.html",
        email_template_name='contas/password_reset_email.html',
        subject_template_name='contas/password_reset_subject.txt',
        success_url=reverse_lazy('contas:password_reset_done')
    ), name="password_reset"),
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(
        template_name="contas/reset_password_sent.html"
    ), name="password_reset_done"),
    path('reset/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(
        template_name="contas/recover_password.html",
        success_url=reverse_lazy('contas:password_reset_complete')
    ), name="password_reset_confirm"),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name="contas/reset_password_complete.html"
    ), name="password_reset_complete"),
]

# correção namespace reset passord
# https://coderoad.ru/52293502/NoReverseMatch-%D0%BD%D0%B0-accounts-password_reset
# https://simpleisbetterthancomplex.com/tutorial/2016/09/19/how-to-create-password-reset-view.html


# Documentação consultada:
# https://docs.djangoproject.com/en/2.2/topics/auth/default/
