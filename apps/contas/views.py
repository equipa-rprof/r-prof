from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserAdminCreationForm, UserFotoChangeForm, UserChangeForm
from .models import User
from apps.gestao_escolar.views import side_bar_func


def register(request):
    if request.method == "POST":
        form = UserAdminCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"Nova conta criada com sucesso: '{username}''")
            login(request, user, 'apps.contas.backends.EmailBackend')
            messages.success(request, f"Efetuou login com sucesso como '{username}'")
            return redirect("gestao_escolar:school_year")
        else:
            if form.error_messages:
                messages.error(request, "Falha no registo! Verifique os campos do formulário.")
            template_name = "contas/register.html"
            context = {"form": form}
            return render(request, template_name, context)

    form = UserAdminCreationForm
    return render(request=request, template_name="contas/register.html", context={"form": form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, f"Efetuou login com sucesso como '{username}'")
                return redirect('gestao_escolar:school_year')
            else:
                messages.error(request, "Utilizador e/ou password inválidos.")
        else:
            messages.error(request, "Utilizador e/ou password inválidos.")
    form = AuthenticationForm()
    template_name = "contas/login.html"
    context = {"form": form}
    return render(request, template_name, context)


def logout_view(request):
    logout(request)
    messages.success(request, "Efetuou logout com sucesso!")
    return redirect("landing_page:landing_page")


def terms(request):
    template_name = 'contas/terms.html'
    return render(request, template_name)


@login_required
def user_profile_change(request, user_slug):
    """
        Apresenta os dados do utilizador e permite que este os altere.
        # link -> navbar -> icon configurações -> "alterar dados"
        path("perfil/<user_slug>/alterar", views.user_profile_change,
         name="user_profile_change"),
    """
    user = User.objects.get(slug=user_slug)

    form = UserChangeForm(instance=user)

    if request.method == 'POST':
        form = UserChangeForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect('gestao_escolar:homepage_blank')

    template_name = 'contas/user_profile_change.html'
    context = {
        'form': form,
    }

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def user_profile_foto_change(request, user_slug):
    """
        Apresenta a foto do utilizador e permite que este a altere.
        # link -> navbar -> icon configurações -> "alterar foto"
        # path("perfil/<user_slug>/alterar_foto", views.user_profile_foto_change, name="user_profile_foto_change"),
    """

    user = User.objects.get(slug=user_slug)

    form = UserFotoChangeForm(instance=user)

    if request.method == 'POST':
        form = UserFotoChangeForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            form.save()
            return redirect('gestao_escolar:homepage_blank')

    template_name = 'contas/user_profile_foto_change.html'
    context = {
            'form': form,
        }

    context = side_bar_func(request, context)

    return render(request, template_name, context)


@login_required
def change_password(request):
    """
        Apresenta a formulário para alteração de password do utilizador.
        # link -> navbar -> icon configurações -> "alterar password"
        path("change_password/", views.change_password, name="change_password"),
    """
    # colocar este url
    # path("change_password/", views.change_password, name="change_password"),

    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Importante!
            messages.success(request, 'A sua password foi atualizada com sucesso!')
            return redirect('contas:change_password')
        else:
            messages.error(request, "Corrija os erros abaixo indicados!")
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'contas/change_password.html', {
        'form': form
    })
