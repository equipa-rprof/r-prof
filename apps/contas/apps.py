from django.apps import AppConfig


class UtilizadoresConfig(AppConfig):
    name = 'apps.contas'
    verbose_name = 'Contas'
