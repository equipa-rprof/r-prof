from django.test import TestCase, Client
from django.urls import reverse

from apps.contas.models import User


class ContasViewTest(TestCase):
    """Tests for the contas/views"""

    def setUp(self):
        """Method called before every test function to set up any objects that may be modified by the test"""
        self.client = Client()
        self.user = User.objects.create(
            username='max', first_name='Maximiniano', last_name='Rodrigues', email='max@gmail.com', address='Lisboa',
            phone='944444444')
        self.user.set_password('1234')
        self.user.save()
        self.client.login(username='max', password='1234')

    # todo
    # passar para a gestão escolar
    # def test_homepage_if_url_exists_at_desired_location(self):
    #     response = self.client.get('/home/')
    #     self.assertEqual(response.status_code, 200)
    # todo
    # passar para a gestão escolar
    # def test_homepage_view_url_accessible_by_name(self):
    #     response = self.client.get(reverse('homepage'))
    #     self.assertEqual(response.status_code, 200)
    # todo
    # passar para a gestão escolar
    # def test_homepage_view_uses_correct_template(self):
    #     response = self.client.get(reverse('homepage'))
    #     self.assertTemplateUsed(response, 'base.html')

    def test_logout_if_urls_exists_at_desired_locations(self):
        self.client.logout()
        response = self.client.get('/contas/logout/')
        # verifies if redirects to another page (landing_page)
        self.assertEqual(response.status_code, 302)
        # verifies if gets the "landing_page" after logout
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_logout_view_url_accessible_by_name(self):
        self.client.logout()
        response = self.client.get(reverse('contas:logout'))
        # verifies if redirects to another page (landing_page)
        self.assertEqual(response.status_code, 302)
        # verifies if gets the "landing_page" after logout
        response = self.client.get(reverse('landing_page:landing_page'))
        self.assertEqual(response.status_code, 200)

    def test_logout_view_redirect_to_correct_template(self):
        self.client.logout()
        # verifies if redirects to another page (landing_page)
        response = self.client.get(reverse('contas:logout'))
        self.assertRedirects(response, '/')
        # verifies if gets de correct template
        response = self.client.get(reverse('landing_page:landing_page'))
        self.assertTemplateUsed(response, 'landing_page/landing_page.html')

    def test_login_if_url_exists_at_desired_location(self):
        self.client.logout()
        # user login, post method
        response = self.client.post('/contas/login/', data={'username': 'max', 'password': '1234'})
        # verifies if redirects to another homepage
        self.assertEqual(response.status_code, 302)
        # verifies if gets the "landing_page" after logout
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_login_verify_if_detects_none_user(self):
        self.client.logout()
        # user login, post method
        self.user = None
        self.client.post(reverse('contas:login'), self.user)
        self.assertNotIn('_auth_user_id', self.client.session)

    def test_login_verify_if_user_can_login(self):
        self.client.logout()
        # user login, post method
        self.client.post(reverse('contas:login'), data={'username': 'max', 'password': '1234'})
        self.assertIn('_auth_user_id', self.client.session)

    def test_login_verify_when_user_is_not_valid(self):
        self.client.logout()
        # user login, post method
        response = self.client.post('/contas/login/', data={'username': 'wrong_user', 'password': '1234'})
        messages = list(response.context['messages'])
        self.assertEqual(str(messages[0]), 'Utilizador e/ou password inválidos.')

    def test_register_if_user_can_do_the_register(self):
        self.client.logout()
        # user register, post method
        response = self.client.post('/contas/register/',
                                    data={'username': 'rita', 'email': 'rita@gmail.com', 'password1': '1##R$%TG234',
                                          'password2': '1##R$%TG234'})
        # verifies if redirects to login and homepage
        self.assertEqual(response.status_code, 302)

    def test_register_if_user_cant_do_the_register_invalid_password(self):
        self.client.logout()
        # user register, post method
        response = self.client.post('/contas/register/',
                                    data={'username': 'rita', 'email': 'rita@gmail.com', 'password1': '1234',
                                          'password2': '1234'})
        # verifies if redirects to login and homepage
        self.assertEqual(response.status_code, 200)

    def test_register_if_url_exists_at_desired_location(self):
        self.client.logout()
        # user register, post method
        response = self.client.get('/contas/register/')
        # verifies if goes to register_page
        self.assertEqual(response.status_code, 200)

    def test_terms_if_url_exists_at_desired_location(self):
        self.client.logout()
        # user register, post method
        response = self.client.get('/contas/terms/')
        # verifies if goes to terms page
        self.assertEqual(response.status_code, 200)

## *** Consulted documentation ****
# Writing and running tests:
# https://docs.djangoproject.com/en/2.2/topics/testing/overview/
