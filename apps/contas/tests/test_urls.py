from django.test import SimpleTestCase
from django.urls import reverse, resolve
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, \
    PasswordResetCompleteView

from apps.contas.views import login_view, logout_view, register


class TestUrls(SimpleTestCase):

    #Todo
    # passar esta função para a gestão escolar
    # def test_homepage_url_resolves(self):
    #     url = reverse('homepage')
    #     self.assertEquals(resolve(url).func, homepage)

    def test_login_view_url_resolves(self):
        url = reverse('contas:login')
        self.assertEquals(resolve(url).func, login_view)

    def test_logout_view_url_resolves(self):
        url = reverse('contas:logout')
        self.assertEquals(resolve(url).func, logout_view)

    def test_register_url_resolves(self):
        url = reverse('contas:register')
        self.assertEquals(resolve(url).func, register)

    def test_password_reset_url_resolves(self):
        url = reverse('contas:password_reset')
        self.assertEquals(
            resolve(url).func.view_class, PasswordResetView
        )

    def test_password_reset_done_url_resolves(self):
        url = reverse('contas:password_reset_done')
        self.assertEquals(
            resolve(url).func.view_class, PasswordResetDoneView
        )

    def test_password_reset_confirm_url_resolves(self):
        url = reverse('contas:password_reset_confirm', args=['uid', 'token'])
        self.assertEquals(
            resolve(url).func.view_class, PasswordResetConfirmView
        )

    def test_password_reset_complete_url_resolves(self):
        url = reverse('contas:password_reset_complete')
        self.assertEquals(
            resolve(url).func.view_class, PasswordResetCompleteView
        )

## *** Consulted documentation ****
# Testing tools
# https://docs.djangoproject.com/en/2.2/topics/testing/tools/