from django.test import TestCase

from apps.contas.models import User


class UserTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        """Set up non-modified objects used by all test methods"""

        User.objects.create(username='max', first_name='Maximiniano', last_name='Rodrigues', email='max@gmail.com',
                            address='Lisboa', phone='944444444')
        User.objects.create(username='marquinhos', last_name='Marques', email='marques@gmail.com',
                            address='Olhão', phone='955555555')
        User.objects.create(username='jony', first_name='João', email='joao@gmail.com',
                            address='Viseu', phone='966666666')

    @classmethod
    def tearDownClass(cls):
        User.objects.all().delete()

    def test_username_label(self):
        user = User.objects.get(id=1)
        field_label = user._meta.get_field('username').verbose_name
        self.assertEqual(field_label, 'Username')

    def test_get_full_name_method(self):
        user1 = User.objects.get(id=1)
        user2 = User.objects.get(id=2)
        user3 = User.objects.get(id=3)
        self.assertEqual(user1.get_full_name(), 'Maximiniano Rodrigues')
        self.assertEqual(user2.get_full_name(), 'Marques')
        self.assertEqual(user3.get_full_name(), 'João')

    def test_get_short_name_method(self):
        user1 = User.objects.get(id=1)
        user2 = User.objects.get(id=2)
        user3 = User.objects.get(id=3)
        self.assertEqual(user1.get_short_name(), 'Maximiniano')
        self.assertEqual(user2.get_short_name(), '')
        self.assertEqual(user3.get_short_name(), 'João')

    def test_get_str_method(self):
        user1 = User.objects.get(id=1)
        user2 = User.objects.get(id=2)
        self.assertEqual(user1.__str__(), 'Maximiniano')
        self.assertEqual(user2.__str__(), 'marquinhos')

## *** Consulted documentation ****
# Writing and running tests:
# https://docs.djangoproject.com/en/2.2/topics/testing/overview/

