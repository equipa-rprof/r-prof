from django.test import TestCase, RequestFactory

from apps.contas.backends import EmailBackend
from apps.contas.models import User


class EmailBackendTest(TestCase):
    """Test for the custom email Backend"""

    def setUp(self):
        """Method called before every test function to set up any objects that may be modified by the test"""
        self.factory = RequestFactory()
        self.user = User.objects.create(username='max', first_name='Maximiniano', last_name='Rodrigues',
                                        email='max@gmail.com', address='Lisboa', phone='944444444')
        self.user.set_password('1234')
        self.user.save()

    def test_email_authenticate_returns_a_user(self):
        """verify if a user can authenticate by email"""
        request = self.factory.get('/home')
        self.assertEqual(
            EmailBackend.authenticate(self, request, username='max@gmail.com', password='1234'), self.user
        )

    def test_email_authenticate_returns_none_with_wrong_password(self):
        """verify if a user can authenticate by email and wrong password"""
        request = self.factory.get('/home')
        self.assertEqual(
            EmailBackend.authenticate(self, request, username='max@gmail.com', password='abcd'), None
        )

    def test_email_authenticate_returns_none_user_does_not_exist(self):
        """verify if a AnonymousUser user can authenticate by email"""
        request = self.factory.get('/home')
        self.assertEqual(
            EmailBackend.authenticate(self, request, username='anonimo@gmail.com', password='1234'), None
        )


# *** Consulted documentation ****
# Writing and running tests:
# https://docs.djangoproject.com/en/2.2/topics/testing/overview/

# Class RequestFactory:
# https://docs.djangoproject.com/en/2.2/topics/testing/advanced/

# Customizing authentication in Django:
# https://docs.djangoproject.com/en/2.2/topics/auth/customizing/


