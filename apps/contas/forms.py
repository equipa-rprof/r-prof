from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import User


class UserAdminCreationForm(UserCreationForm):
    """ formulário de criação de utilizador """

    class Meta:
        model = User
        fields = ['username', 'email']


class UserAdminForm(forms.ModelForm):
    """ formulário de configuração de mais dados do utilizador no 'Admin' """

    class Meta:
        model = User
        fields = [
            'username', 'email', 'first_name', 'last_name', 'address', 'phone',
            'is_active', 'is_staff', 'avatar', 'slug',
        ]


class UserChangeForm(forms.ModelForm):
    """ formulário para configuração de dados do utilizador"""

    class Meta:
        model = User
        fields = [
            'first_name', 'last_name', 'address', 'phone',
        ]


class UserFotoChangeForm(forms.ModelForm):
    """ formulário para atualização de foto do utilizador"""

    class Meta:
        model = User
        fields = ['avatar', ]
